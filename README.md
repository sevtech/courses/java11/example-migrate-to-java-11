# example-migrate-to-java-11
## Pasos para migrar de Java 8 a Java 11
Este proyecto es la imagen de como debería quedar el proyecto *example-java-8* con el sistema de modulos de Java 9.

Al igual que el proyecto antes mencionado se compila mediante:
~~~~
mvn clean install
~~~~

Levanta el recurso GET:
 ~~~~
 localhost:8080/user
~~~~
