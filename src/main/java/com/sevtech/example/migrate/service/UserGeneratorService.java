package com.sevtech.example.migrate.service;

import com.sevtech.example.migrate.model.User;
import org.springframework.stereotype.Service;

@Service
public class UserGeneratorService {

    public User generateUser() {
        return new User(26,"Jesus","Jimenez",70000);
    }
}
