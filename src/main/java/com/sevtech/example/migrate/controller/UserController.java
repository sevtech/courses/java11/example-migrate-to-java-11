package com.sevtech.example.migrate.controller;

import com.sevtech.example.migrate.model.User;
import com.sevtech.example.migrate.service.UserGeneratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserGeneratorService service;

    @GetMapping
    public ResponseEntity<User> getUser() {
        return ResponseEntity.ok(service.generateUser());
    }

}
