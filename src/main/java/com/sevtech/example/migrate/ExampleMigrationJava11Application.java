package com.sevtech.example.migrate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExampleMigrationJava11Application {

	public static void main(String[] args) {
		SpringApplication.run(ExampleMigrationJava11Application.class, args);
	}

}
