package com.sevtech.example.migrate.model;

public class User {

    private int age;
    private String name;
    private String surname;
    private long salary;

    public User (int age, String name, String surname, long salary){
        this.age = age;
        this.name = name;
        this.surname = surname;
        this.salary = salary;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public long getSalary() {
        return salary;
    }

    public void setSalary(long salary) {
        this.salary = salary;
    }
}
